FROM python@sha256:ee2da4ef36f8b0f3bf076450712ab797418fc68676fa7686490ab307a7ddbdcd

WORKDIR /zanarkand

COPY requirements.txt .

RUN apt update && \
    apt install -y ffmpeg && \
    pip install -r requirements.txt

ADD agency-fb-bold.ttf /usr/share/fonts/agency-fb-bold/
COPY zanarkand_ffmpeg.py .

ENTRYPOINT ["python", "/zanarkand/zanarkand_ffmpeg.py"]