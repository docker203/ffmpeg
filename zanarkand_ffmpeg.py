#! /usr/bin/python3

# Standard Imports
import os
import sys
import logging

# Third Party Imports
from discord_webhook import DiscordWebhook
import ffmpeg

def stream_episode():
    overlay = ffmpeg.input('/resources/overlay.png')\
                    .filter('ass',
                            filename='/resources/final.ass')
    video = ffmpeg.input(f"/media/{os.environ['FFMPEG_PLAYLIST']}/{os.environ['FFMPEG_PLAYLIST']}-E{os.environ['FFMPEG_EPISODE']}.v",
                         re=None)\
                  .video\
                  .filter('scale',
                          os.environ['FFMPEG_VIEWPORT_WIDTH'],
                          os.environ['FFMPEG_VIEWPORT_HEIGHT'])\
                  .filter('pad',
                          os.environ['FFMPEG_RESOLUTION_WIDTH'],
                          os.environ['FFMPEG_RESOLUTION_HEIGHT'],
                          os.environ['FFMPEG_VIEWPORT_X'],
                          os.environ['FFMPEG_VIEWPORT_Y'])\
                  .overlay(overlay)
    audio = ffmpeg.input(f"/media/{os.environ['FFMPEG_PLAYLIST']}/{os.environ['FFMPEG_PLAYLIST']}-E{os.environ['FFMPEG_EPISODE']}.a",
                         re=None)
    logging.info('Streaming episode %s-E%s', os.environ['FFMPEG_PLAYLIST'], os.environ['FFMPEG_EPISODE'])
    try:
        ffmpeg.output(video,
                      audio,
                      f"rtmp://a.rtmp.youtube.com/live2/{os.environ['YOUTUBE_KEY']}",
                      format=os.environ['FFMPEG_FORMAT'],
                      vcodec=os.environ['FFMPEG_VCODEC'],
                      acodec=os.environ['FFMPEG_ACODEC'],
                      minrate=os.environ['FFMPEG_MINRATE'],
                      maxrate=os.environ['FFMPEG_MAXRATE'],
                      bufsize=os.environ['FFMPEG_BUFSIZE'],
                      crf=os.environ['FFMPEG_CRF'],
                      preset=os.environ['FFMPEG_PRESET'],
                      audio_bitrate=os.environ['FFMPEG_AUDIO_BITRATE'],
                      ar=os.environ['FFMPEG_AR'],
                      g=os.environ['FFMPEG_G'])\
               .run(capture_stdout=False,
                    capture_stderr=True,
                    quiet=True)
    except ffmpeg.Error as err:
        logging.error('Error while streaming %s-E%s', os.environ['FFMPEG_PLAYLIST'], os.environ['FFMPEG_EPISODE'])
        DiscordWebhook(url=os.environ['DISCORD_WEBHOOK'], content=err.stderr.decode('utf-8')[-2000:]).execute()


def main():
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s %(levelname)s: %(message)s',
                        datefmt='%d-%b-%y %H:%M:%S')
    required_files = ['/resources/overlay.png',
                      '/resources/final.ass']
    for f in required_files:
        if not os.path.isfile(f):
            logging.error('File %s does not exist. Exiting', f)
            sys.exit(1)
    stream_episode()

if __name__ == "__main__":
    main()
